<?php get_header(); ?>

	<?php $page_for_posts = get_option('page_for_posts'); ?>

	<section class="hero">
		<div class="content">

			<div class="photo">
				<img src="<?php $image = get_field('hero_image', $page_for_posts); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		
			<div class="headline page-title">
				<div class="wrapper">			

					<h1><?php the_field('hero_headline', $page_for_posts); ?></h1>

				</div>
			</div>
			
		</div>

		<div class="angle">
			<img src="<?php bloginfo('template_directory') ?>/images/hero-angle.png" alt="" />
		</div>
	</section>


	<section class="main">
		<div class="wrapper">

			<div class="filters">
				<div class="select-wrapper">
					<?php wp_dropdown_categories( 'show_option_none=Filter by Topic&orderby=name' ); ?>
				</div>
				
				
				<script type="text/javascript">
				    var dropdown = document.getElementById("cat");
				    function onCatChange() {
				        if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
				            location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
				        } else {
				            location.href = "<?php echo esc_url( home_url( '/blog/' ) ); ?>";
				        }
				    }
				    dropdown.onchange = onCatChange;
				</script>
			</div>

			<div class="posts">
				<?php if(is_category()): ?>

					<?php
						$current_cat = get_category(get_query_var('cat'),false);

						echo do_shortcode('[ajax_load_more container_type="div" category="' . $current_cat->slug . '" css_classes="three-col blog" theme_repeater="blog.php" post_type="post" posts_per_page="8" scroll="false" transition_container="false" button_label="Load More Posts"]');
					?>	

				<?php else: ?>

					<?php echo do_shortcode('[ajax_load_more container_type="div" css_classes="three-col blog" theme_repeater="blog.php" post_type="post" posts_per_page="8" scroll="false" transition_container="false" button_label="Load More Posts"]'); ?>	

				<?php endif; ?>			
			</div>			

		</div>
	</section>


<?php get_footer(); ?>