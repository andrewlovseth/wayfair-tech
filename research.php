<?php

/*

	Template Name: Research

*/

get_header(); ?>
		
	<?php get_template_part('partials/hero'); ?>

	<section class="main">

		<section class="what-we-do">
			<div class="wrapper">

				<div class="section-header">
					<h2><?php the_field('what_we_do_headline'); ?></h2>
				</div>

				<div class="copy deck p3">
					<?php the_field('what_we_do_deck'); ?>
				</div>

			</div>
		</section>


		<section class="current-projects">
			<div class="wrapper">

				<div class="section-header">
					<h2><?php the_field('current_projects_headline'); ?></h2>
				</div>

				<div class="copy deck p3">
					<?php the_field('current_projects_deck'); ?>
				</div>

				<div class="projects three-col three-col-center">
					
					<?php if(have_rows('current_projects')): while(have_rows('current_projects')): the_row(); ?>
					 
					    <div class="grid-item project">
					    	<div class="project-wrapper content">
					    		<div class="icon">
					    			<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    		</div>

					    		<div class="headline">
					    			<h4><?php the_sub_field('name'); ?></h4>
					    		</div>
					    	</div>
					        
					    </div>

					<?php endwhile; endif; ?>

				</div>

			</div>
		</section>

		<section class="funding">
			<div class="wrapper">
				
				<div class="section-header align-left">
					<h2><?php the_field('funding_headline'); ?></h2>
				</div>

				<div class="two-col-info">
					<div class="col tabs-container">

						<?php if(have_rows('funding_tabs')): ?>
							<div class="tab-links">
								
								<?php $i = 1; while(have_rows('funding_tabs')): the_row(); ?>

							    	<a href="#tab-<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>"<?php if($i == 1): ?> class="active"<?php endif; ?>><?php the_sub_field('title'); ?></a>

						    	<?php $i++; endwhile; ?>

						    </div>
						<?php endif; ?>

						<?php if(have_rows('funding_tabs')): ?>
							<div class="tabs">
								
								<?php $i = 1; while(have_rows('funding_tabs')): the_row(); ?>

									<div class="tab<?php if($i == 1): ?> active<?php endif; ?>" id="tab-<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">
										<?php the_sub_field('copy'); ?>										
									</div>
	
						    	<?php $i++; endwhile; ?>

						    </div>
						<?php endif; ?>
						
					</div>

					<div class="col inset">
						<div class="headline">
							<h4><?php the_field('proposal_headline'); ?></h4>
						</div>

						<div class="copy p3">
							<?php the_field('proposal_copy'); ?>
						</div>

						<div class="cta">
							<a href="<?php the_field('proposal_link'); ?>" class="btn btn-purple">Submit Here</a>
						</div>
					</div>
				</div>

			</div>
		</section>

	</section>

<?php get_footer(); ?>