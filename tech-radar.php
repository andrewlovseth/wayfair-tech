<?php

/*

	Template Name: Tech Radar

*/

get_header(); ?>
		
	<?php get_template_part('partials/hero'); ?>

	<section class="main">

		<section class="radar">
			<div class="wrapper">

				<a href="<?php the_field('radar_link'); ?>" rel="external">
					<img src="<?php $image = get_field('radar_fpo_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>		

			</div>
		</section>

		<section class="radar-description">
			<div class="wrapper">
				
				<div class="two-col-info">
					<div class="col">
						<div class="copy p3">
							<?php the_field('radar_description_col_1'); ?>
						</div>						
					</div>

					<div class="col">
						<div class="copy p3">
							<?php the_field('radar_description_col_2'); ?>
						</div>						
					</div>
				</div>	

			</div>
		</section>

	</section>

<?php get_footer(); ?>