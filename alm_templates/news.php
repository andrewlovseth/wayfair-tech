<article class="blog news">
	<a href="<?php the_field('link'); ?>" target="_blank">

		<div class="photo">
			<div class="content">
				<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'medium'); ?>" alt="" />
			</div>
		</div>

		<div class="info">
			<div class="headline">
				<h4><?php the_title(); ?></h4>
			</div>

			<div class="meta">
				<h5><?php the_time('F j, Y'); ?></h5>
			</div>

			<div class="excerpt copy p3">
				<p><?php the_content(); ?></p>
			</div>
		</div>
		
	</a>
</article>