<article class="event">
	<div class="photo">
		<div class="content">
			<a href="<?php the_permalink(); ?>">
				<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'thumbnail'); ?>" alt="" />
			</a>
		</div>
	</div>

	<div class="info">
		<div class="headline">
			<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
		</div>

		<?php
			$start_date_field = get_field('start_date');
			$start_date = new DateTime($start_date_field);

			$end_date_field = get_field('end_date');
			$end_date = new DateTime($end_date_field);

			if($start_date_field == $end_date_field) {
				$event_date = $start_date->format('F j, Y');
			} else {
				$event_date = $start_date->format('F j, Y') . ' - ' . $end_date->format('F j, Y');
			}
		?>

		<div class="meta">
			<h4>
				<?php echo $event_date; ?>
				<?php if(get_field('location')): ?> | <?php the_field('location'); ?><?php endif; ?>
				<?php if(get_field('time')): ?> | <?php the_field('time'); ?><?php endif; ?>	
			</h4>
		</div>		
	</div>
	
</article>