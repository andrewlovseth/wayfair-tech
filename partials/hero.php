<section class="hero">
	<div class="content">

		<div class="photo">
			<?php if(get_field('hero_headline')): ?>
				<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php else: ?>
				<?php $page_for_posts = get_option('page_for_posts'); ?>

				<img src="<?php $image = get_field('hero_image', $page_for_posts); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			<?php endif; ?>
		</div>
	
		<div class="headline page-title">
			<div class="wrapper">

				<?php if(get_field('hero_headline')): ?>
					<h1><?php the_field('hero_headline'); ?></h1>
				<?php else: ?>
					<h1><?php the_title(); ?></h1>
				<?php endif; ?>	

			</div>
		</div>
		
	</div>

	<div class="angle">
		<img src="<?php bloginfo('template_directory') ?>/images/hero-angle.png" alt="" />
	</div>
</section>