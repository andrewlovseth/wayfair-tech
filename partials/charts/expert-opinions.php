<script type="text/javascript">
// Set a callback to run when the Google Visualization API is loaded.

google.charts.setOnLoadCallback(drawExpertOpinionsChart);
// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawExpertOpinionsChart() {
<?php if( have_rows('expert_opinions') ): ?>
  // Create the data table.
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Opinions');
  data.addColumn('number', 'Number of Employees');
  data.addRows([
  <?php while ( have_rows('expert_opinions') ) : the_row(); ?>
    ['<?php the_sub_field('opinion'); ?>', <?php the_sub_field('number_of_employees'); ?>],
  <?php endwhile; ?>
  ]);
  <?php else :
    // no rows found
  endif; ?>
  if (window.matchMedia("(min-width: 640px)").matches) {
    // Set chart options
  var options = {
    'legend': {
      'position': 'labeled',
      'alignment': 'start',
      'textStyle': {
        'color': '#681A53',
        'fontSize': 20
        }
      },

    'pieSliceText': 'none',
    'enableInteractivity': false,
    'chartArea':{left:'10%',top:20,width:'80%',height:'80%'},
    <?php if( have_rows('expert_opinions') ):
    $p == 0; ?>
      slices: {
        <?php while ( have_rows('expert_opinions') ) : the_row();
        $p++; ?>
        <?php echo ($p - 1); ?>: { color: '<?php the_sub_field("color"); ?>' },

        <?php endwhile; ?>
      }
    <?php else :
      // no rows found
    endif; ?>
   };
  } else {
    // Set chart options
  var options = {
    'legend': {
      'position': 'labeled',
      'alignment': 'start',
      'textStyle': {
        'color': '#681A53',
        'fontSize': 10
        }
      },

    'pieSliceText': 'none',
    'enableInteractivity': false,
    'chartArea':{left:'0%',top:20,width:'100%',height:'40%'},
    <?php if( have_rows('expert_opinions') ):
    $pp == 0; ?>
      slices: {
        <?php while ( have_rows('expert_opinions') ) : the_row();
        $pp++; ?>
        <?php echo ($pp - 1); ?>: { color: '<?php the_sub_field("color"); ?>' },

        <?php endwhile; ?>
      }
    <?php else :
      // no rows found
    endif; ?>
   };

  }


  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.PieChart(document.getElementById('chart_expert_opinions'));
  chart.draw(data, options);

}
</script>

<!--Div that will hold the pie chart-->
<div id="chart_expert_opinions" style="width: 100%; height: 400px;"></div>