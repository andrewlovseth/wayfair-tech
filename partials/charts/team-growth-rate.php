<script type="text/javascript">

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawTeamGrowthRateChart() {
  <?php if( have_rows('team_growth_rate') ): ?>
  // Create the data table.
  var data = google.visualization.arrayToDataTable([
     ['Team Growth Rate', 'Number of Employees', { role: 'style' }],
     <?php while ( have_rows('team_growth_rate') ) : the_row(); ?>
        ['<?php the_sub_field('year'); ?>', <?php the_sub_field('number_of_employees'); ?>, 'color: <?php the_sub_field('color'); ?>'],
      <?php endwhile; ?>


  ]);

    <?php else :
      // no rows found
    endif; ?>
    if (window.matchMedia("(min-width: 640px)").matches) {
      // Set chart options
      var options = {
        'legend': {'position': 'none'},
        'chartArea':{top:20,width:'80%',height:'60%'},
        'enableInteractivity': false,
        hAxis: {
          title: 'Year'
        },
        vAxis: {
          title: 'Count of Team Members',
        },
      }

    } else {
      // Set chart options
      var options = {
        'legend': {'position': 'none'},
        'chartArea':{top:20,left: '20%', width:'80%',height:'60%'},
        'enableInteractivity': false,
        hAxis: {
          title: 'Year',
          slantedText:true,
          slantedTextAngle:30,
        },
        vAxis: {
          title: 'Count of Team Members',
        },
      }
    }



  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.ColumnChart(document.getElementById('chart_team_growth_rate'));
  chart.draw(data, options);

//  function selectHandler() {
//   var selectedItem = chart.getSelection()[0];
//   if (selectedItem) {
//     var value = data.getValue(selectedItem.row, selectedItem.column);
//     alert('The user selected ' + value);
//   }
// }

// // Listen for the 'select' event, and call my function selectHandler() when
// // the user selects something on the chart.
// google.visualization.events.addListener(chart, 'select', selectHandler);
}
</script>

<!--Div that will hold the pie chart-->
<div id="chart_team_growth_rate" style="width: 100%; height: 400px;"></div>