<script type="text/javascript">
  jQuery(document).ready( function($) {
// Set a callback to run when the Google Visualization API is loaded.

google.charts.setOnLoadCallback(drawDisciplinaryBackgroundChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawDisciplinaryBackgroundChart() {
  <?php if( have_rows('discipline') ): ?>
  // Create the data table.
  var data = google.visualization.arrayToDataTable([
     ['Disciplinary Background', 'Number of Employees', { role: 'style' }],
     <?php while ( have_rows('discipline') ) : the_row(); ?>
        ['<?php the_sub_field('discipline'); ?>', <?php the_sub_field('number_of_employees'); ?>, 'color: <?php the_sub_field('color'); ?>'],
      <?php endwhile; ?>


  ]);

    <?php else :
      // no rows found
    endif; ?>

   if ($(window).width() < 641) {
    // Set chart options
    var options = {
      'legend': {'position': 'none'},
       hAxis: {
        slantedText:true,
        slantedTextAngle:90,
        title: 'Field of Study'
      },
      vAxis: {
        title: 'Count of Team Members',
      },
      'chartArea':{top:20,width:'70%',height:'40%'},
      'enableInteractivity': false,
      }
    }
    else {
      // Set chart options
      var options = {
        'legend': {'position': 'none'},
         hAxis: {
          slantedText:true,
          slantedTextAngle:30,
          title: 'Field of Study'
        },
        vAxis: {
          title: 'Count of Team Members',
        },
        'chartArea':{top:20,width:'80%',height:'60%'},
        'enableInteractivity': false,
      }
    }



  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.ColumnChart(document.getElementById('chart_disciplinary_background'));
  chart.draw(data, options);

//  function selectHandler() {
//   var selectedItem = chart.getSelection()[0];
//   if (selectedItem) {
//     var value = data.getValue(selectedItem.row, selectedItem.column);
//     alert('The user selected ' + value);
//   }
// }

// // Listen for the 'select' event, and call my function selectHandler() when
// // the user selects something on the chart.
// google.visualization.events.addListener(chart, 'select', selectHandler);
}
});
</script>

<!--Div that will hold the pie chart-->
<div id="chart_disciplinary_background" style="width: 100%; height: 500px;"></div>