<script type="text/javascript">
// Set a callback to run when the Google Visualization API is loaded.

google.charts.setOnLoadCallback(drawCareerPathChart);
// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawCareerPathChart() {
  <?php if( have_rows('career_paths') ): ?>
  // Create the data table.
var data = google.visualization.arrayToDataTable([
     ['Field of Work', 'Number of Employees', { role: 'style' }],
     <?php while ( have_rows('career_paths') ) : the_row(); ?>
        ['<?php the_sub_field('work_field'); ?>', <?php the_sub_field('number_of_employees'); ?>, 'color: <?php the_sub_field('color'); ?>'],
      <?php endwhile; ?>
  ]);
 <?php else :
    // no rows found
  endif; ?>

  if (window.matchMedia("(min-width: 640px)").matches) {
   var options =
    {
    'legend': {'position': 'none'},
    hAxis: {
      title: 'Count of Team Members',
      minValue: 0
    },
    vAxis: {
      title: 'Field of Work'
    },
    'chartArea':{top:20, left: '40%', width: '50%', height: '80%'},
    'enableInteractivity': false,

  }

    } else {
      var options =
        {
        'legend': {'position': 'none'},
        hAxis: {
          title: 'Count of Team Members',
          minValue: 0,
        },
        vAxis: {
          title: 'Field of Work',
          slantedText:true,
          slantedTextAngle:40,
        },
        'chartArea':{top:20, left: '60%', width: '35%', height: '80%'},
        'enableInteractivity': false,

      }

    }

  // Set chart options

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.BarChart(document.getElementById('chart_career_path'));
  chart.draw(data, options);


}
</script>

<!--Div that will hold the pie chart-->
<div id="chart_career_path" style="width: 100%; height: 400px;"></div>