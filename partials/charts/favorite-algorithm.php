<script type="text/javascript">
// Set a callback to run when the Google Visualization API is loaded.

google.charts.setOnLoadCallback(drawFavoriteAlgorithmChart);
// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawFavoriteAlgorithmChart() {

  // Create the data table.
 var data = google.visualization.arrayToDataTable([
          ['Favorite ML Algorithm', 'Height'],
          [ 'XGBoost',      12],
          [ 'Regression',      5.5],
          [ 'Decision Tree',     14],
          [ 'Naive Bayes',     14],
          [ 'Naive Bayes',     12],
          [ 'Other',     14],

        ]);

  var options =
    {
    'chartArea':{top:20},
    'legend': {'position': 'none'},
    hAxis: {
      title: 'Favorite ML Algorithm',
      minValue: 0
    },
    vAxis: {
      title: 'Height'
    }

  }

  // Instantiate and draw our chart, passing in some options.
  var chart = new google.visualization.ScatterChart(document.getElementById('chart_favorite_algorithm'));
  chart.draw(data, options);


}
</script>

<!--Div that will hold the pie chart-->
<div id="chart_favorite_algorithm" style="width: 100%; height: 600px;"></div>