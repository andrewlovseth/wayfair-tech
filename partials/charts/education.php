<script type="text/javascript">

  // Set a callback to run when the Google Visualization API is loaded.
  google.charts.setOnLoadCallback(drawEducationChart);

  // Callback that creates and populates a data table,
  // instantiates the pie chart, passes in the data and
  // draws it.
  function drawEducationChart() {
    <?php if( have_rows('education_levels') ): ?>
    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Level');
    data.addColumn('number', 'Number of Employees');
    data.addRows([
    <?php while ( have_rows('education_levels') ) : the_row(); ?>
      ['<?php the_sub_field('level'); ?>', <?php the_sub_field('number_of_employees'); ?>],
    <?php endwhile; ?>
    ]);
    <?php else :
      // no rows found
    endif; ?>

    // Set chart options
    var options = {
      'legend': 'labeled',
      'pieSliceText': 'none',
      'pieHole': 0.6,
      'chartArea':{left:'10%',top:20,width:'80%',height:'80%'},
      'enableInteractivity': false,
      <?php if( have_rows('education_levels') ):
      $s == 0; ?>
        slices: {
          <?php while ( have_rows('education_levels') ) : the_row();
          $s++; ?>
          <?php echo ($s - 1); ?>: { color: '<?php the_sub_field("color"); ?>' },

          <?php endwhile; ?>
        }
      <?php else :
        // no rows found
      endif; ?>
     };

    // Instantiate and draw our chart, passing in some options.
    var chartEducation = new google.visualization.PieChart(document.getElementById('chart_education'));

    // function selectHandlerEducation() {
    //   var selectedItemEducation = chartEducation.getSelection()[0];
    //   if (selectedItemEducation) {
    //     var level = data.getValue(selectedItemEducation.row, 0);
    //     // alert('The user selected ' + level);
    //      $("#education-popup").addClass('animated fadeInRight');
    //      $("#education-popup").css('display', 'inline-block');
    //     //  $('.slick-slider-education').slick({
    //     //   dots: true,
    //     //   slidesToShow: 3,
    //     //   slidesToScroll: 1,
    //     // });
    //   }
    // }

    // google.visualization.events.addListener(chartEducation, 'select', selectHandlerEducation);
    console.log('data', data);
    chartEducation.draw(data, options);
  }

</script>
<div id="chart_education" style="width: 100%; height: 400px;"></div>
