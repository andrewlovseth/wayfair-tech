<nav class="mobile-nav">
	<div class="scroll">

		<div class="mobile-nav-logo">
			<a href="<?php echo site_url('/'); ?>">
				<img src="<?php $image = get_field('site_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>

		<?php wp_nav_menu(array('menu' => 'Header')); ?>

		<div class="mobile-search">
			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
		        <input type="search" class="search-query" autocomplete="off" placeholder="Search..." name="s" />
		    </form>	
		</div>
		
	</div>
</nav>