<section class="team-members people">
	<div class="wrapper">
		
		<div class="section-header">
			<h2><?php the_field('team_members_header'); ?></h2>
		</div>

		<div class="team-members-wrapper people-wrapper four-col four-col-center">
			<?php $team_members = get_field('team_members'); if( $team_members ): ?>
				<?php foreach( $team_members as $team_member ): ?>

					<div class="person team-member grid-item">
						<div class="photo">
							<a href="#" class="bio-trigger" data-bio="<?php echo get_permalink($team_member->ID); ?>">
								<?php $image = get_field('photo', $team_member->ID); if($image): ?>
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								<?php else: ?>
									<img class="wayfair-profile" src="<?php echo get_stylesheet_directory_uri(); ?>/images/wayfair-profile.png" alt="Wayfair" />
								<?php endif; ?>
							</a>
						</div>

						<div class="info">
							<div class="headline">
								<h3><a href="#" class="bio-trigger" data-bio="<?php echo get_permalink($team_member->ID); ?>"><?php echo get_the_title($team_member->ID ); ?></a></h3>
								<h4><?php the_field('title', $team_member->ID); ?></h4>
								<h5><?php the_field('degree', $team_member->ID); ?>, <br/><?php the_field('university', $team_member->ID); ?></h5>

							</div>
						</div>

						<div class="cta">
							<a href="#" class="btn btn-purple bio-trigger" data-bio="<?php echo get_permalink($team_member->ID); ?>">Read More</a>
						</div>
					</div>

				<?php endforeach; ?>
			<?php endif; ?>
		</div>

	</div>
</section>
