<div class="filters">
	<div class="filters-wrapper">
		
		<a href="#" data-filter="" class="all active">All Projects</a>	

		<?php $project_tags = get_terms( 'post_tag', array( 'hide_empty' => false ) ); foreach ( $project_tags as $project_tag ): ?>

			<a href="#" data-filter=".<?php echo $project_tag->slug; ?>"><?php echo $project_tag->name; ?></a>
			
		<?php endforeach; ?>
	</div>
</div>