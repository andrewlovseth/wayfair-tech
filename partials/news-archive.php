	<section class="hero">
		<div class="content">

			<div class="photo">
				<img src="<?php $image = get_field('news_hero_image', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		
			<div class="headline page-title">
				<div class="wrapper">			

					<h1><?php the_field('news_hero_headline', 'options'); ?></h1>

				</div>
			</div>
			
		</div>

		<div class="angle">
			<img src="<?php bloginfo('template_directory') ?>/images/hero-angle.png" alt="" />
		</div>
	</section>


	<section class="main">
		<div class="wrapper">

			<div class="filters">
				<div class="select-wrapper">
					<?php if(is_tax('news_type')): $news_type = get_queried_object(); ?>

						<?php wp_dropdown_categories('show_option_none=Filter by Type&orderby=name&taxonomy=news_type&value_field=slug&selected=' . $news_type->slug ); ?>

					<?php else: ?>

						<?php wp_dropdown_categories('show_option_none=Filter by Type&orderby=name&taxonomy=news_type&value_field=slug'); ?>

					<?php endif; ?>
				</div>
								
				<script type="text/javascript">
				    var dropdown = document.getElementById("cat");
				    function onCatChange() {
				        if ( dropdown.options[dropdown.selectedIndex].value != -1 ) {
				            location.href = "<?php echo esc_url( home_url( '/' ) ); ?>news-type/"+dropdown.options[dropdown.selectedIndex].value;
				        } else {
				            location.href = "<?php echo esc_url( home_url( '/news/' ) ); ?>";
				        }
				    }
				    dropdown.onchange = onCatChange;
				</script>
			</div>

			<div class="posts">
				<?php if(is_tax('news_type')): ?>

					<?php
						$news_type = get_queried_object();

						echo do_shortcode('[ajax_load_more container_type="div" post_type="news"  taxonomy="news_type" taxonomy_terms="' . $news_type->slug . '" taxonomy_operator="IN" css_classes="three-col blog" theme_repeater="news.php" posts_per_page="6" scroll="false" transition_container="false" button_label="View More News"]');
					?>	

				<?php else: ?>

					<?php echo do_shortcode('[ajax_load_more container_type="div" css_classes="three-col blog" theme_repeater="news.php" post_type="news" posts_per_page="6" scroll="false" transition_container="false" button_label="View More News"]'); ?>	

				<?php endif; ?>			
			</div>			

		</div>
	</section>