<section class="leaders people">
	<div class="wrapper">
		
		<div class="section-header">
			<h2><?php the_field('leaders_header'); ?></h2>
		</div>

		<div class="leaders-wrapper people-wrapper four-col four-col-center">
			<?php $leaders = get_field('leaders'); if( $leaders ): ?>
				<?php foreach( $leaders as $leader ): ?>

					<div class="person leader grid-item">
						<div class="photo">
							<a href="#" class="bio-trigger" data-bio="<?php echo get_permalink($leader->ID); ?>">
								<?php $image = get_field('photo', $leader->ID); if($image): ?>
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								<?php else: ?>
									<img class="wayfair-profile" src="<?php echo get_stylesheet_directory_uri(); ?>/images/wayfair-profile.png" alt="Wayfair" />
								<?php endif; ?>
							</a>
						</div>

						<div class="info">
							<div class="headline">
								<h3><a href="#" class="bio-trigger" data-bio="<?php echo get_permalink($leader->ID); ?>"><?php echo get_the_title($leader->ID ); ?></a></h3>
								<h4><?php the_field('title', $leader->ID); ?></h4>
								<h5><?php the_field('degree', $leader->ID); ?>, <br/><?php the_field('university', $leader->ID); ?></h5>
							</div>
						</div>

						<div class="cta">
							<a href="#" class="btn btn-purple bio-trigger" data-bio="<?php echo get_permalink($leader->ID); ?>">Read More</a>
						</div>
					</div>

				<?php endforeach; ?>
			<?php endif; ?>
		</div>

	</div>
</section>
