<div id="search-overlay">
	<div id="search-overlay-wrapper">
		<a href="#" class="search-close"><img src="<?php bloginfo('template_directory') ?>/images/icon-close.svg" alt="Close" /></a>

		<div class="search-wrapper">

			<div class="headline">
				<h3>Search...</h3>
			</div>
			
			<div class="modal-search">
				<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			        <input type="search" class="search-query" autocomplete="off" placeholder="Search..." name="s" />
			    </form>	
			</div>

		</div>
		
	</div>
</div>

      