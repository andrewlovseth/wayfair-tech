<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page">

	<header class="site-header">
		<div class="wrapper">

			<div class="site-logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('site_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="desktop-nav">
				<?php wp_nav_menu(array('menu' => 'Header')); ?>

				<div class="search-icon">
					<a href="#" class="search-trigger">
						<img src="<?php bloginfo('template_directory') ?>/images/icon-search.svg" alt="Search" />
					</a>

				</div>			
			</div>

			<div class="ctas">
				<div class="cta subscribe">
					<a href="<?php the_field('subscribe_link', 'options'); ?>" class="btn btn-white" rel="external">Subscribe</a>
				</div>

				<div class="cta subscribe">
					<a href="<?php the_field('work_with_us_link', 'options'); ?>" class="btn btn-purple" rel="external">Work With Us</a>
				</div>
			</div>
			
			<div class="hamburger">
				<a href="#" class="nav-trigger">
					<div class="patty"></div>
				</a>
			</div>

		</div>
	</header>

	<?php get_template_part('partials/header/mobile-nav'); ?>