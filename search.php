<?php get_header(); ?>

	<?php if ( have_posts() ): ?>

			<?php
				$page_for_posts = get_option('page_for_posts');
				$search_query = get_search_query();
			?>

			<section class="hero">
				<div class="content">

					<div class="photo">
						<img src="<?php $image = get_field('hero_image', $page_for_posts); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				
					<div class="headline page-title">
						<div class="wrapper">			

							<h1>Search Results: <?php echo $search_query; ?></h1>

						</div>
					</div>
					
				</div>

				<div class="angle">
					<img src="<?php bloginfo('template_directory') ?>/images/hero-angle.png" alt="" />
				</div>
			</section>

			<section class="results">
				<div class="wrapper">

					<?php while ( have_posts() ): the_post(); ?>

						<article>
							<div class="photo">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail( 'thumbnail' ); ?>
								</a>
							</div>
							<div class="info">
								<div class="headline">
									<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
								</div>

								<div class="copy p3">
									<?php
										$content = get_the_content();
										$clean_content = strip_tags($content);
										$content_length = strlen($clean_content);
										if($content_length > 250) :
										  $excerpt = substr($clean_content, 0, 250) . '...';
										else :
										  $excerpt = $clean_content;
										endif;
									?>
									<p><?php echo esc_html($excerpt); ?></p>
								</div>
							</div>

						</article>

					<?php endwhile; ?>

				</div>
			</section>


	<?php else: ?>

		<section class="no-results">
			<div class="wrapper">

				<div class="info">
					<div class="headline">
						<h1>No Results</h1>
					</div>

					<div class="copy p1">
						<p>Sorry, but the search term you entered didn’t find any results.<br/>
						Perhaps try searching the term data science.</p>
					</div>
				</div>

				<div class="form">
					<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
				        <input type="search" class="search-query" autocomplete="off" placeholder="Search..." name="s" />
				    </form>	
				</div>
				
			</div>
		</section>

	<?php endif; ?>

<?php get_footer(); ?>