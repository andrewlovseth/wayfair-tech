<?php

/*

	Template Name: Open Source

*/

get_header(); ?>
		
	<?php get_template_part('partials/hero'); ?>

	<section class="main">

		<section class="mission">
			<div class="wrapper">
				
				<div class="section-header">
					<h2><?php the_field('mission_headline'); ?></h2>
				</div>

				<div class="copy p2 deck">
					<?php the_field('mission_deck'); ?>
				</div>

			</div>
		</section>

		<section class="open-source-projects">
			<div class="wrapper">
				
				<div class="section-header">
					<h2><?php the_field('open_source_projects_headline'); ?></h2>
				</div>

				<?php // get_template_part('partials/open-source/filters'); ?>

				<div class="three-col projects">

					<?php if(have_rows('open_source_projects')): ?>
							
						<?php while(have_rows('open_source_projects')): the_row(); ?>

							<?php $terms = get_sub_field('tags'); $tags = ''; if( $terms ): ?>
								<?php foreach( $terms as $term ): ?>

									<?php $tags .= $term->slug . ' '; ?>

								<?php endforeach; ?>
							<?php endif; ?>

							<div class="project active <?php echo $tags; ?>">
								<?php if(get_sub_field('icon')): ?>
									<div class="icon">
										<a href="<?php the_sub_field('link'); ?>" rel="external">
											<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										</a>
									</div>
								<?php endif; ?>

								<div class="headline">
									<h4>
										<a href="<?php the_sub_field('link'); ?>" rel="external">
											<?php the_sub_field('title'); ?>	
										</a>
									</h4>
								</div>

								<div class="copy p3">
									<?php the_sub_field('description'); ?>
								</div>

								<div class="link">
									<a href="<?php the_sub_field('link'); ?>" rel="external">GitHub</a>
								</div>
																	
							</div>

				    	<?php endwhile; ?>

					<?php endif; ?>
					
				</div>

			</div>
		</section>

		<?php $posts = get_field('blog_posts'); if( $posts ): ?>
			<section class="blog">
				<div class="wrapper">

					<div class="section-header">
						<h2><?php the_field('blog_header'); ?></h2>
					</div>

					<div class="posts three-col">
					
						<?php foreach( $posts as $p ): ?>
							<article class="blog">
								<a href="<?php echo get_permalink($p->ID); ?>">

									<div class="photo">
										<div class="content">
											<img src="<?php echo get_the_post_thumbnail_url($p->ID, 'medium'); ?>" alt="">
										</div>
									</div>

									<div class="info">
										<div class="headline">
											<h4><?php echo get_the_title($p->ID); ?></h4>
										</div>

										<div class="meta">
											<h5><?php echo get_the_time('F j, Y', $p->ID); ?></h5>
										</div>

										<div class="excerpt copy p3">
											<p><?php
												$content = get_the_content(null, false, $p->ID);
												echo wp_trim_words( $content , '20' ); 
												?></p>
										</div>
									</div>
									
								</a>
							</article>
						<?php endforeach; ?>

					</div>

					<div class="cta">
						<?php 
						$link = get_field('blog_cta');
						if( $link ): 
						    $link_url = $link['url'];
						    $link_title = $link['title'];
						    $link_target = $link['target'] ? $link['target'] : '_self';
						    ?>
						    <a class="btn btn-purple" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						<?php endif; ?>
					</div>
					
				</div>
			</section>
		<?php endif; ?>

	</section>

<?php get_footer(); ?>