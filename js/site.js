(function ($, window, document, undefined) {

	$(document).ready(function($) {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});

		// Menu Toggle
		$('.nav-trigger').click(function(){
			$('body').toggleClass('nav-overlay-open');
			return false;
		});

		// Desktop Nav Cancel
		$('.desktop-nav .menu-item-has-children > a').click(function(){
			return false;
		});

		// Desktop Nav Hover
		$('.desktop-nav .menu-item-has-children').hover(

			// On Mouse Over
			function() {
				$(this).addClass('hover');
			},

			// On Mouse Out
			function() {
				$(this).removeClass('hover');
			}
		);

		// Mobile Nav Toggle
		$('.mobile-nav .menu-item-has-children > a').click(function(){
			$(this).parent().toggleClass('subnav-toggle');
			return false;
		});


		// Bio Trigger
		$('.bio-trigger').on('click', function() {
			$('#bio-overlay #profile-wrapper').empty();

			var profile_url = $(this).data('bio');
			$('#bio-overlay #profile-wrapper').load(profile_url + ' #profile');

			$('body').toggleClass('bio-open');
			return false;
		});

		// Bio Close
		$('.bio-close').click(function(){
			$('body').toggleClass('bio-open');
			return false;
		});


		// Search Trigger
		$('.search-trigger').click(function(){
			$('body').toggleClass('search-open');
			return false;
		});

		// Search Close
		$('.search-close').click(function(){
			$('body').toggleClass('search-open');
			return false;
		});


		// Tab Links
		$('.tab-links a').on('click', function() {

			var tab_target = $(this).attr('href');

			$('.tabs .tab').removeClass('active')
			$(tab_target).addClass('active');


			$('.tab-links a').removeClass('active')
			$(this).addClass('active');

			return false;
		});



		// Open Source Filters
		$('.filters a').on('click', function() {

			var filter_target = $(this).data('filter');
			console.log(filter_target);

			$('.project').removeClass('active');
			$('.project' + filter_target).addClass('active');

			$('.filters a').removeClass('active');
			$(this).addClass('active');

			return false;
		});



		// FitVids
		$('.video-wrapper').fitVids();


		// Workstreams
		$('.workstreams-slick-slider').slick({
			dots: true,
			arrows: false,
		    slidesToShow: 3,
		    slidesToScroll: 1,
		    centerMode: true,
		    centerPadding: '0px',
			autoplay: true,
			autoplaySpeed: 5000,
		});

		$('.workstreams-slick-slider').on('click', '.slick-slide', function (e) {
			e.stopPropagation();
			var index = $(this).data("slick-index");
			if ($('.slick-slider').slick('slickCurrentSlide') !== index) {
				$('.slick-slider').slick('slickGoTo', index);
			}
		});


		// Team Slider
		$('body.page-template-team .people-wrapper').slick({
			dots: true,
			arrows: true,
			mobileFirst: true,
		    slidesToShow: 1,
		    slidesToScroll: 1,
			responsive: [
				{
				  breakpoint: 768,
				  settings: {
				    slidesToShow: 4,
				    slidesToScroll: 4,

				  }
				},
				{
				  breakpoint: 568,
				  settings: {
				    slidesToShow: 2,
				    slidesToScroll: 2
				  }
				}
			]
		});


		// Escape -- Kill All Modals/Navs
		$(document).keyup(function(e) {
			
			if (e.keyCode == 27) {
				$('body').removeClass('bio-open nav-overlay-open search-open');
			}

		});

	});



	// Sticky Nav

	$(window).scroll(function() {    
	    var scroll = jQuery(window).scrollTop();
		
		if (scroll >= 300) {
			jQuery('body').addClass('sticky');

		} else {
			jQuery('body').removeClass('sticky');
		}

	}); 



})(jQuery, window, document);