<?php

/*

	Template Name: Culture

*/

get_header(); ?>
		
	<?php get_template_part('partials/hero'); ?>

	<section class="main">

		<section class="values">
			<div class="wrapper">
				
				<div class="section-header">
					<h2><?php the_field('values_header'); ?></h2>
				</div>

				<div class="values-wrapper four-col four-col-center">
					<?php if(have_rows('values')): while(have_rows('values')): the_row(); ?>
					 
						<div class="value grid-item">
							<div class="photo">
								<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="info">
								<div class="headline">
									<h4><?php the_sub_field('headline'); ?></h4>
								</div>

								<div class="copy p3">
									<?php the_sub_field('description'); ?>
								</div>
							</div>
						</div>

					<?php endwhile; endif; ?>
				</div>

			</div>
		</section>

<!--
		<section class="how-we-work">
			<div class="wrapper">

				<div class="section-header">
					<h2><?php the_field('how_we_work_header'); ?></h2>
				</div>

			</div>
		</section>
-->

		<section class="leaders people">
			<div class="wrapper">
				
				<div class="section-header">
					<h2><?php the_field('people_header'); ?></h2>
				</div>

				<div class="leaders-wrapper three-col three-col-center">
					<?php $leaders = get_field('people'); if( $leaders ): ?>
						<?php foreach( $leaders as $leader ): ?>

							<div class="person leader grid-item">
								<div class="photo">
									<a href="#" class="bio-trigger" data-bio="<?php echo get_permalink($leader->ID); ?>">
										<?php $image = get_field('photo', $leader->ID); if($image): ?>
											<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
										<?php else: ?>
											<img class="wayfair-profile" src="<?php echo get_stylesheet_directory_uri(); ?>/images/wayfair-profile.png" alt="Wayfair" />
										<?php endif; ?>
									</a>
								</div>

								<div class="info">
									<div class="headline">
										<h3><a href="#" class="bio-trigger" data-bio="<?php echo get_permalink($leader->ID); ?>"><?php echo get_the_title($leader->ID ); ?></a></h3>
										<h4><?php the_field('tagline', $leader->ID); ?></h4>
									</div>

									<div class="copy p3 quote">
										<?php the_field('quote', $leader->ID); ?>
									</div>
								</div>

								<div class="cta">
									<a href="#" class="btn btn-purple bio-trigger" data-bio="<?php echo get_permalink($leader->ID); ?>">Read More</a>
								</div>
							</div>

						<?php endforeach; ?>
					<?php endif; ?>
				</div>

			</div>
		</section>


	</section>

<?php get_footer(); ?>