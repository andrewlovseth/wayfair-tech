<?php

/*

	Template Name: Home

*/

get_header(); ?>

		
	<?php get_template_part('partials/hero'); ?>


	<section class="about">
		<div class="wrapper">
			
			<div class="two-col-headline-copy">
				<div class="headline">
					<h2><?php the_field('about_headline'); ?></h2>
				</div>

				<div class="copy p2">
					<?php the_field('about_copy'); ?>
				</div>				
			</div>

		</div>
	</section>


	<section class="teams">
		<div class="wrapper">
		
			<div class="section-header">
				<h2><?php the_field('teams_header'); ?></h2>
			</div>

			<div class="teams-wrapper">

				<?php if(have_rows('teams')): while(have_rows('teams')): the_row(); ?>

					<?php $page_link = get_sub_field('page_link'); ?>
				 
				    <div class="team">
				    	<div class="info">
				    		<div class="info-wrapper">
					    		<div class="headline">
					    			<h3><a href="<?php echo $page_link; ?>"><?php the_sub_field('name'); ?></a></h3>
					    		</div>

					    		<div class="copy p3">
					    			<?php the_sub_field('description'); ?>
					    		</div>
				    		</div>
				    	</div>

				    	<div class="photo">
				    		<a href="<?php echo $page_link; ?>">
				    			<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				    		</a>
				    	</div>

				    </div>

				<?php endwhile; endif; ?>
				
			</div>

		</div>
	</section>

	
	<?php get_template_part('partials/leaders'); ?>


	<section class="values">
		<div class="wrapper">
			
			<div class="section-header">
				<h2><?php the_field('values_header'); ?></h2>
			</div>

			<div class="values-wrapper four-col four-col-center">
				<?php if(have_rows('values')): while(have_rows('values')): the_row(); ?>
				 
					<div class="value grid-item">
						<div class="photo">
							<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>

						<div class="info">
							<div class="headline">
								<h4><?php the_sub_field('headline'); ?></h4>
							</div>

							<div class="copy p3">
								<?php the_sub_field('description'); ?>
							</div>
						</div>
					</div>

				<?php endwhile; endif; ?>
			</div>

			<div class="cta">
				<?php $values_link = get_field('values_link'); ?>

				<a href="<?php echo $values_link; ?>" class="btn btn-purple">Learn More</a>
			</div>

		</div>
	</section>


<?php get_footer(); ?>