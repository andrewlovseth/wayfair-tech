<?php

/*

	Template Name: Team

*/

get_header(); ?>
	
	<?php get_template_part('partials/hero'); ?>

	<section class="main">

		<section class="what-we-do">
			<div class="wrapper">
				
				<div class="two-col-headline-copy">
					<div class="headline">
						<h2><?php the_field('what_we_do_header'); ?></h2>
					</div>

					<div class="copy p2">
						<div class="copy-wrapper">
							<?php the_field('what_we_do_copy'); ?>
						</div>						
					</div>				
				</div>

			</div>
		</section>

		<?php if(have_rows('workstreams')): ?>
			<section class="workstreams">
				<div class="wrapper">

					<div class="workstreams-slick-slider">
						<?php while(have_rows('workstreams')): the_row(); ?>
						 
						    <div class="workstream">
						    	<div class="icon">
						    		<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						    	</div>

						    	<div class="info">
						    		<div class="headline">
						    			<h4><?php the_sub_field('title'); ?></h4>
						    		</div>

						    		<div class="copy p3">
						    			<?php the_sub_field('copy'); ?>
						    		</div>						    		
						    	</div>						        
						    </div>
						<?php endwhile; ?>
					</div>
					
				</div>
			</section>
		<?php endif; ?>

		<?php if(get_field('show_tech_stack')): ?>
			<section class="tech-stack">
				<div class="wrapper">
						
					<div class="section-header">
						<h2><?php the_field('tech_stack_header'); ?></h2>
					</div>

					<div class="copy p3 p3-18">
						<?php the_field('tech_stack_copy'); ?>
					</div>

				</div>
			</section>
		<?php endif; ?>

		<?php get_template_part('partials/team-leaders'); ?>

		<?php get_template_part('partials/team-members'); ?>

		<?php $posts = get_field('blog_posts'); if( $posts ): ?>
			<section class="blog">
				<div class="wrapper">

					<div class="section-header">
						<h2><?php the_field('blog_header'); ?></h2>
					</div>

					<div class="posts three-col">
					
						<?php foreach( $posts as $p ): ?>
							<article class="blog">
								<a href="<?php echo get_permalink($p->ID); ?>">

									<div class="photo">
										<div class="content">
											<img src="<?php echo get_the_post_thumbnail_url($p->ID, 'medium'); ?>" alt="">
										</div>
									</div>

									<div class="info">
										<div class="headline">
											<h4><?php echo get_the_title($p->ID); ?></h4>
										</div>

										<div class="meta">
											<h5><?php echo get_the_time('F j, Y', $p->ID); ?></h5>
										</div>

										<div class="excerpt copy p3">
											<p><?php
												$content = get_the_content(null, false, $p->ID);
												echo wp_trim_words( $content , '20' ); 
												?></p>
										</div>
									</div>
									
								</a>
							</article>
						<?php endforeach; ?>

					</div>
					
				</div>
			</section>
		<?php endif; ?>

		<section class="join-us">
			<div class="wrapper">
				
				<div class="two-col-headline-copy">
					<div class="headline">
						<h2><?php the_field('join_us_header'); ?></h2>
					</div>

					<div class="copy p2">
						<div class="copy-wrapper">
							<?php the_field('join_us_copy'); ?>
						</div>						
					</div>				
				</div>

				<div class="cta">
					<?php 
					$link = get_field('join_us_cta');
					if( $link ): 
					    $link_url = $link['url'];
					    $link_title = $link['title'];
					    $link_target = $link['target'] ? $link['target'] : '_self';
					    ?>
					    <a class="btn btn-purple" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					<?php endif; ?>

				</div>

			</div>
		</section>

	</section>

<?php get_footer(); ?>