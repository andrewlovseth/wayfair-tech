<?php

/*

	Template Name: 3D University

*/

get_header(); ?>
		
	<?php get_template_part('partials/hero'); ?>

	<section class="main">

		<section class="about">
			<div class="wrapper">

				<div class="section-header align-left">
					<h2><?php the_field('about_headline'); ?></h2>
				</div>

				<div class="info tabs-container">
					<?php if(have_rows('about_tabs')): ?>
						<div class="tab-links">
							
							<?php  $i = 1; while(have_rows('about_tabs')): the_row(); ?>

						    	<a href="#tab-<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>"<?php if($i == 1): ?> class="active"<?php endif; ?>>
						    		<?php the_sub_field('title'); ?>
					    		</a>

					    	<?php $i++; endwhile; ?>

					    </div>
					<?php endif; ?>

					<?php if(have_rows('about_tabs')): ?>
						<div class="tabs">
							
							<?php  $i = 1; while(have_rows('about_tabs')): the_row(); ?>

								<div class="tab<?php if($i == 1): ?> active<?php endif; ?>" id="tab-<?php echo sanitize_title_with_dashes(get_sub_field('title')); ?>">
									<div class="copy p3">
										<?php the_sub_field('copy'); ?>
									</div>																			
								</div>

					    	<?php $i++;  endwhile; ?>

					    </div>
					<?php endif; ?>

				</div>

			</div>
		</section>

		<section class="learn-more">
			<div class="wrapper">

				<div class="section-header align-left">
					<h2><?php the_field('learn_more_headline'); ?></h2>
				</div>

				<div class="three-col features">
					
					<?php if(have_rows('learn_more')): while(have_rows('learn_more')): the_row(); ?>
					 
					    <div class="grid-item feature">
					    	<div class="feature-wrapper content">
					    		<div class="icon">
					    			<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    		</div>

					    		<div class="headline">
					    			<h4><?php the_sub_field('name'); ?></h4>
					    		</div>
					    	</div>
					        
					    </div>

					<?php endwhile; endif; ?>

				</div>

			</div>
		</section>

		<section class="get-involved">
			<div class="wrapper">
				
				<div class="section-header align-left">
					<h2><?php the_field('get_involved_headline'); ?></h2>
				</div>

				<div class="copy p3">
					<?php the_field('get_involved_copy'); ?>
				</div>
			</div>
		</section>

	</section>

<?php get_footer(); ?>