<?php get_header(); ?>

	<section class="hero">
		<div class="content">

			<div class="photo">
				<img src="<?php $image = get_field('events_hero_image', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		
			<div class="headline page-title">
				<div class="wrapper">	
					<h1><?php the_field('events_hero_headline', 'options'); ?></h1>
				</div>
			</div>
			
		</div>

		<div class="angle">
			<img src="<?php bloginfo('template_directory') ?>/images/hero-angle.png" alt="" />
		</div>
	</section>

	<section class="main">
		<div class="wrapper">

			<div class="col tabs-container">
				<div class="tab-links">
					<a href="#tab-upcoming" class="active">Upcoming Events</a>
					<a href="#tab-past">Past Events</a>
			    </div>

				<div class="tabs">					

					<div class="tab active" id="tab-upcoming">

						<?php
							$today = date('Ymd');
							$args = array(
								'post_type' => 'events',
								'posts_per_page' => 6,
								'meta_key' => 'end_date',
								'meta_value' => $today,
								'order' => 'ASC',
								'orderby' => 'meta_value_num',
								'meta_compare' => '>=',
								'meta_type' => 'DATE'
							);
							$query = new WP_Query( $args );
							if ( $query->have_posts() ) : ?>

							<?php		
								echo do_shortcode('[ajax_load_more theme_repeater="event.php" post_type="events" posts_per_page="6" meta_key="end_date" meta_value="' . $today . '" order="ASC" orderby="meta_value_num" meta_compare="&gt;=" meta_type="DATE" scroll="false" transition_container="false" button_label="Load More Events" button_loading_label="Loading Events..."]');
							?>

						<?php else: ?>

							<div class="no-results copy p2">
								<p>There are currently no upcoming events.</p>
							</div>


						<?php endif; wp_reset_postdata(); ?>	

					</div>

					<div class="tab" id="tab-past">

						<?php
							$today = date('Ymd');
							$args = array(
								'post_type' => 'events',
								'posts_per_page' => 6,
								'meta_key' => 'end_date',
								'meta_value' => $today,
								'order' => 'DESC',
								'orderby' => 'meta_value_num',
								'meta_compare' => '<',
								'meta_type' => 'DATE'
							);
							$query = new WP_Query( $args );
							if ( $query->have_posts() ) : ?>

							<?php		
							echo do_shortcode('[ajax_load_more theme_repeater="event.php" post_type="events" posts_per_page="6" meta_key="end_date" meta_value="' . $today . '"  order="DESC" orderby="meta_value_num" meta_compare="&lt;=" meta_type="DATE" scroll="false" transition_container="false" button_label="Load More Events" button_loading_label="Loading Events..."]');
							?>

						<?php else: ?>

							<div class="no-results copy p2">
								<p>There are no past events.</p>
							</div>


						<?php endif; wp_reset_postdata(); ?>	

					</div>

			    </div>				
			</div>

		</div>
	</section>

<?php get_footer(); ?>