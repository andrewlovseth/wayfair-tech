<?php

/*

	Template Name: Wayfair Next

*/

get_header(); ?>
		
	<?php get_template_part('partials/hero'); ?>

	<section class="main">

		<section class="innovation">
			<div class="wrapper">

				<div class="section-header">
					<h2><?php the_field('innovation_header'); ?></h2>
				</div>

				<div class="copy p3 deck">
					<?php the_field('innovation_deck'); ?>
				</div>

				<div class="features-wrapper">
					<?php if(have_rows('innovation_features')): while(have_rows('innovation_features')): the_row(); ?>

					 	<div class="feature">
					    	<div class="info">
					    		<div class="info-wrapper">
						    		<div class="headline">
						    			<h3><?php the_sub_field('name'); ?></h3>
						    		</div>

						    		<div class="copy p3">
						    			<?php the_sub_field('description'); ?>
						    		</div>
					    		</div>
					    	</div>

					    	<div class="photo">
					    		<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    	</div>

					    </div>

					<?php endwhile; endif; ?>					
				</div>

				<div class="video">
					<div class="video-wrapper">

						<?php the_field('innovation_video'); ?>
						
					</div>
				</div>

			</div>
		</section>

		<?php get_template_part('partials/leaders'); ?>

	</section>

<?php get_footer(); ?>