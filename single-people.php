<?php get_header(); ?>

	<section class="main">
		<div class="wrapper">

			<div id="profile">
				<div class="photo">
					<?php $image = get_field('photo'); if($image): ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php else: ?>
						<img class="wayfair-profile" src="<?php echo get_stylesheet_directory_uri(); ?>/images/wayfair-profile.png" alt="Wayfair" />
					<?php endif; ?>
				</div>

				<div class="info">
					<div class="headline">
						<h3><?php the_title(); ?></h3>
						<h4><?php the_field('title'); ?></h4>
					</div>

					<div class="copy p2">
						<?php the_field('bio'); ?>
					</div>
				</div>
			</div>

		</div>
	</section>

<?php get_footer(); ?>