<?php get_header(); ?>

	<?php $page_for_posts = get_option('page_for_posts'); ?>

	<section class="hero">
		<div class="content">

			<div class="photo">
				<img src="<?php $image = get_field('hero_image', $page_for_posts); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		
			<div class="headline page-title">
				<div class="wrapper">			

					<h1><?php the_field('hero_headline', $page_for_posts); ?></h1>

				</div>
			</div>
			
		</div>

		<div class="angle">
			<img src="<?php bloginfo('template_directory') ?>/images/hero-angle.png" alt="" />
		</div>
	</section>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
		
	<section class="main">
		<div class="wrapper">
				<article>

					<section class="article-header">
						<div class="headline">
							<h2><?php the_title(); ?></h2>
						</div>

						<div class="meta">
							<h5><?php the_time('F j, Y'); ?></h5>
						</div>						

						<div class="photo featured-image">
							<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>" alt="">
						</div>

						<div class="authors">
							<?php $posts = get_field('authors'); if( $posts ): ?>
								<?php foreach( $posts as $p ): ?>
									<div class="author">
										<div class="photo">
											<?php if(get_field('photo', $p->ID)): ?>
												<img src="<?php $image = get_field('photo', $p->ID ); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
											<?php else: ?>
												<img class="wayfair-profile" src="<?php echo get_stylesheet_directory_uri(); ?>/images/wayfair-profile.png" alt="Wayfair" />
											<?php endif; ?>
										</div>
										<div class="name">
											<h5><?php echo get_the_title( $p->ID ); ?></h5>
										</div>
									</div>
								<?php endforeach; ?>
							<?php endif; ?>														
						</div>

						<?php get_template_part('partials/blog/social-share'); ?>

					</section>

					<section class="article-body copy p3">
						<?php the_content(); ?>					
					</section>

					<section class="article-footer">
						<div class="categories">
							<?php the_category( ' ' ); ?>
						</div>
					</section>				

				</article>
			</section>

			<section class="comments-section">
				<div class="wrapper">
					
					<?php comments_template(); ?> 

				</div>
			</section>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>