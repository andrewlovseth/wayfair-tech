	<footer class="site-footer">

		<section class="connect">
			<div class="wrapper">

				<div class="footer-logo">
					<a href="<?php echo site_url('/'); ?>">
						<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</a>
				</div>

				<div class="ctas">
					<div class="cta subscribe">
						<a href="<?php the_field('subscribe_link', 'options'); ?>" class="btn btn-purple" rel="external">Subscribe for Updates</a>
					</div>

					<div class="cta subscribe">
						<a href="<?php the_field('work_with_us_link', 'options'); ?>" class="btn btn-purple" rel="external">Work With Us</a>
					</div>
				</div>

				<div class="social">
					
					<div class="headline">
						<h3>Follow Us</h3>
					</div>

					<div class="links">
						<?php if(have_rows('social_Links', 'options')): while(have_rows('social_Links', 'options')): the_row(); ?>
						 
						    <div class="link">
						        <a href="<?php the_sub_field('link'); ?>" rel="external"><img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
						    </div>

						<?php endwhile; endif; ?>
					</div>

				</div>

			</div>
		</section>

		<section class="copyright">
			<div class="wrapper">

				<div class="copy p4">
					<p><?php the_field('copyright', 'options'); ?></p>
				</div>

			</div>
		</section>

	</footer>

</div> <!-- #page -->

	<?php get_template_part('partials/bio-modal'); ?>
	<?php get_template_part('partials/search-modal'); ?>

	<?php wp_footer(); ?>

</body>
</html>