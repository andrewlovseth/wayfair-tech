<?php get_header(); ?>

	<section class="hero">
		<div class="content">

			<div class="photo">
				<img src="<?php $image = get_field('events_hero_image', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>
		
			<div class="headline page-title">
				<div class="wrapper">	
					<h1><?php the_field('events_hero_headline', 'options'); ?></h1>
				</div>
			</div>
			
		</div>

		<div class="angle">
			<img src="<?php bloginfo('template_directory') ?>/images/hero-angle.png" alt="" />
		</div>
	</section>

	<section class="main">
		<div class="wrapper">

			<section class="event-header">
				<div class="headline">
					<h2><?php the_title(); ?></h2>
				</div>

				<?php
					$start_date_field = get_field('start_date');
					$start_date = new DateTime($start_date_field);

					$end_date_field = get_field('end_date');
					$end_date = new DateTime($end_date_field);

					if($start_date_field == $end_date_field) {
						$event_date = $start_date->format('F j, Y');
					} else {
						$event_date = $start_date->format('F j, Y') . ' - ' . $end_date->format('F j, Y');
					}
				?>

				<div class="meta">
					<h4>
						<?php echo $event_date; ?>
						<?php if(get_field('location')): ?> | <?php the_field('location'); ?><?php endif; ?>
						<?php if(get_field('time')): ?> | <?php the_field('time'); ?><?php endif; ?>	
					</h4>
				</div>

				<div class="photo">
					<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'large'); ?>" alt="" />
				</div>
			</section>

			<section class="event-body article-body copy p3 p3-18">
				<?php the_field('description'); ?>

				<?php 
				$link = get_field('cta');
				if( $link ): 
				    $link_url = $link['url'];
				    $link_title = $link['title'];
				    $link_target = $link['target'] ? $link['target'] : '_self';
				    ?>
				    <div class="cta">
				    	 <a class="btn btn-purple" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
				    </div>
				   
				<?php endif; ?>
			</section>

			<section class="event-footer">

				<div class="back">
					<div class="cta">
						<a href="<?php echo site_url('/events/'); ?>" class="btn btn-purple">Go Back to All Events</a>
					</div>
				</div>
				
			</section>	

		</div>
	</section>

<?php get_footer(); ?>