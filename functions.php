<?php

/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/


// Remove Admin bar from front-end
show_admin_bar( false );


// Theme Support for title tags, post thumbnails, HTML5 elements, feed links
add_theme_support( 'title-tag' );

add_theme_support( 'post-thumbnails' );

add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ));

add_theme_support( 'automatic-feed-links' );

if ( ! function_exists( 'wp_body_open' ) ) {
    function wp_body_open() {
        do_action( 'wp_body_open' );
    }
}

function my_myme_types($mime_types){
$mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);


function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );




/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/

// Register and noConflict jQuery 3.4.1
wp_register_script( 'jquery.3.4.1', 'https://code.jquery.com/jquery-3.4.1.min.js' );
wp_add_inline_script( 'jquery.3.4.1', 'var jQuery = $.noConflict(true);' );

// Enqueue custom styles and scripts
function bearsmith_enqueue_styles_and_scripts() {
    $uri = get_stylesheet_directory_uri();
    $dir = get_stylesheet_directory();

    $script_last_updated_at = filemtime($dir . '/js/site.js');
    $style_last_updated_at = filemtime($dir . '/style.css');

    // Add style.css and third-party css
    // wp_enqueue_style( 'adobe-fonts', 'https://use.typekit.net/vcx3lxt.css' );
    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css', '', $style_last_updated_at );

    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script( 'custom-plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array( 'jquery.3.4.1' ), $script_last_updated_at, true );
    wp_enqueue_script( 'custom-site', get_stylesheet_directory_uri() . '/js/site.js', array( 'jquery.3.4.1' ), $script_last_updated_at, true );
}
add_action( 'wp_enqueue_scripts', 'bearsmith_enqueue_styles_and_scripts' );





add_filter( 'nav_menu_css_class', 'add_slug_class', 10, 2 );
function add_slug_class( $classes, $item ){
    if( 'page' == $item->object ){
        $page = get_post( $item->object_id );
        $classes[] = 'nav-' . $page->post_name;
    }
    if( 'custom' == $item->object ){
        $page = get_post( $item->object_id );
        $classes[] = 'nav-' . $page->post_name;
    }

    return $classes;
}





function my_update_comment_fields( $fields ) {

    $commenter = wp_get_current_commenter();
    $req       = get_option( 'require_name_email' );
    $label     = $req ? '' : ' ' . __( '(optional)', 'text-domain' );
    $aria_req  = $req ? "aria-required='true'" : '';

    $fields['author'] =
        '<p class="comment-form-author">
            <label for="author">' . __( "Name", "text-domain" ) . $label . '</label>
            <input id="author" name="author" type="text" placeholder="' . esc_attr__( "Your name (required)", "text-domain" ) . '" value="' . esc_attr( $commenter['comment_author'] ) .
        '" size="30" ' . $aria_req . ' />
        </p>';

    $fields['email'] =
        '<p class="comment-form-email">
            <label for="email">' . __( "Email", "text-domain" ) . $label . '</label>
            <input id="email" name="email" type="email" placeholder="' . esc_attr__( "Your email (required)", "text-domain" ) . '" value="' . esc_attr( $commenter['comment_author_email'] ) .
        '" size="30" ' . $aria_req . ' />
        </p>';

    return $fields;
}
add_filter( 'comment_form_default_fields', 'my_update_comment_fields' );



/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}


if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page();
    
    acf_add_options_sub_page('Header');
    acf_add_options_sub_page('Footer');
    acf_add_options_sub_page('Events');
    acf_add_options_sub_page('News');

    
}

// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');